import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListarOnibusComponent } from './pages/listar-onibus/listar-onibus.component';
import { ListarLotacaoComponent } from './pages/listar-lotacao/listar-lotacao.component';
import { ItinerarioComponent } from './pages/itinerario/itinerario.component';
import { AboutComponent } from './pages/about/about.component';
import { Error404Component } from './pages/error404/error404.component';

const routes: Routes = [
  { path: 'listarOnibus', component: ListarOnibusComponent },
  { path: 'listarLotacao', component: ListarLotacaoComponent },
  // { path: 'itinerario', component: ItinerarioComponent },
  { path: 'about', component: AboutComponent },
  { path: '', redirectTo: '/about', pathMatch: 'full' },
  { path: '**', component: Error404Component }
];

@NgModule({
  exports: [ RouterModule ],
  imports: [ RouterModule.forRoot(routes) ]
})

export class AppRoutingModule { }
