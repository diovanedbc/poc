import { Linha } from '../model/linha'
import { ServicePadrao } from '../service/servicePadrao'

export class Rota{
  private opcaoRota: string;

  listaBairroCentro: Linha[] = [];
  listaCentroBairro: Linha[] = [];
  itinerario: {};
  private idLinha: string = "";

  constructor(opcaoRota: string, private servicePadrao: ServicePadrao){
    this.opcaoRota = opcaoRota;
    sessionStorage.setItem("idLinha", "");
  }

  public carregaListas(): void{
    const bairroCentro: string = "1";
    const centroBairro: string = "2";

    this.servicePadrao.buscarTodos().subscribe(e =>{
      this.listaBairroCentro = this.filtro(bairroCentro, e);
      this.listaCentroBairro = this.filtro(centroBairro, e);
    })
  }

  public setRota(e: string): void{
    this.opcaoRota = e;
    this.idLinha = "";  
    sessionStorage.setItem("idLinha", this.idLinha);       
  }

  public selecionado(rota: string): boolean{  
    if (!this.opcaoRota) { 
      return false;        
    }     
    return (this.opcaoRota === rota);
  }

  public selecionaItinerario():boolean{
    return sessionStorage.getItem("idLinha") !== "";
  }

  public filtro(tipo: string, lista: Linha[]): Linha[]{      
    return lista.filter(e => {
      let tamanhoCodigo = e.codigo.length;
      let resultado = e.codigo.substring(tamanhoCodigo-1, tamanhoCodigo);

      return resultado === tipo;
    });
  }

  public onChange(e):void {
    this.idLinha = e.target.value;
    sessionStorage.setItem("idLinha", this.idLinha);
  }
}