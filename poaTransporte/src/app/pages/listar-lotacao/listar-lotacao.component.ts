import { Component, OnInit } from '@angular/core';
import { Rota } from '../../model/rota'
import { LotacaoService } from '../../service/lotacaoService'

@Component({
  selector: 'app-listar-lotacao',
  templateUrl: './listar-lotacao.component.html',
  styleUrls: ['./listar-lotacao.component.css'],
  providers: [ LotacaoService ]
})

export class ListarLotacaoComponent extends Rota implements OnInit{
  constructor(lotacaoService: LotacaoService) {
    super("bairroCentro", lotacaoService);
  }

  ngOnInit() {
    this.carregaListas();
  }  
}