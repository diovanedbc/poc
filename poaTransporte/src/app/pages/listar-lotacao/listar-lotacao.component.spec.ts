import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListarLotacaoComponent } from './listar-lotacao.component';

describe('ListarLotacaoComponent', () => {
  let component: ListarLotacaoComponent;
  let fixture: ComponentFixture<ListarLotacaoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarLotacaoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarLotacaoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
