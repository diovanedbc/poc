import { Component, OnInit } from '@angular/core';
import { OnibusService } from '../../service/onibusService';
import { Rota } from '../../model/rota'

@Component({
  selector: 'app-listar-onibus',
  templateUrl: './listar-onibus.component.html',
  styleUrls: ['./listar-onibus.component.css'],
  providers: [ OnibusService ]
})

export class ListarOnibusComponent extends Rota implements OnInit{
  constructor(service: OnibusService) {
    super("bairroCentro", service);
  }

  ngOnInit() {
    this.carregaListas();
  }
}
