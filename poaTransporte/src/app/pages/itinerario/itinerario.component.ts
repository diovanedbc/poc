import { Component, OnInit } from '@angular/core';
import { ItinerarioService } from '../../service/itinerarioService'

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css'],
  providers: [ ItinerarioService ]
})

export class ItinerarioComponent implements OnInit {
  private itinerarios: Array<any> = new Array();
  private idLinha: string;
  private nomeLinha: string;

  constructor(private itinerarioService: ItinerarioService) {
    this.idLinha = sessionStorage.getItem("idLinha");
  }

  ngOnInit() {
    this.carregaItinerarios();
  }

  public carregaItinerarios(): void{
    this.itinerarioService.buscar(this.idLinha).subscribe(e =>{
      let ponto = 0;
      this.nomeLinha = e.nome;
      for(let dado in e){
        let lat = e[dado].lat;
        let lng = e[dado].lng;
        this.itinerarios.push([lat, lng, ponto]);
        ponto++;
      }

      for(let i=0; i<4; i++){
        this.itinerarios.pop();
      }
    })
  }
}
