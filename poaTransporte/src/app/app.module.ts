import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule }    from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ListarOnibusComponent } from './pages/listar-onibus/listar-onibus.component';
import { AboutComponent } from './pages/about/about.component';
import { Error404Component } from './pages/error404/error404.component';
import { ListarLotacaoComponent } from './pages/listar-lotacao/listar-lotacao.component';
import { CabecalhoComponent } from './pages/cabecalho/cabecalho.component';
import { ItinerarioComponent } from './pages/itinerario/itinerario.component';

@NgModule({
  declarations: [
    AppComponent,
    ListarOnibusComponent,
    ListarLotacaoComponent,
    AboutComponent,
    Error404Component,
    CabecalhoComponent,
    ItinerarioComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
