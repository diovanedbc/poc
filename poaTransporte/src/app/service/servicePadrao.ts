import { Linha } from '../model/linha';
import { Observable } from 'rxjs';

export interface ServicePadrao{
  buscarTodos(): Observable<Linha[]>;
}