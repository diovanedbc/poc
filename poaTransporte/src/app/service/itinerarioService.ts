import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class ItinerarioService{

  private url: string = 'http://www.poatransporte.com.br/php/facades/process.php?a=il&p=';

  constructor(private http: HttpClient){ }

  public buscar(id: string): Observable<any> {    
    return this.http.get<any>(`${ this.url + id }`);
  }
}