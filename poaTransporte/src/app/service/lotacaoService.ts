import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Linha } from '../model/linha';
import { ServicePadrao } from '../service/servicePadrao';
import { Observable } from 'rxjs';

@Injectable()
export class LotacaoService implements ServicePadrao{

  private url: string = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=l';

  constructor(private http: HttpClient){ }

  public buscarTodos(): Observable<Linha[]> {
    return this.http.get<Linha[]>(`${ this.url }`);
  }
}