import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Linha } from '../model/linha';
import { Observable } from 'rxjs';
import { ServicePadrao } from './servicePadrao'

@Injectable()
export class OnibusService implements ServicePadrao{

  private url: string = 'http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o';

  constructor(private http: HttpClient){ }

  public buscarTodos(): Observable<Linha[]> {
    return this.http.get<Linha[]>(`${ this.url }`);
  }
}