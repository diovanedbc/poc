package br.com.dbc.poatransporte.Component;

import br.com.dbc.poatransporte.Model.Onibus;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class OnibusComponent {

    public List<Onibus> listaRequisicaoBus(){
        try {
            final String LINK = "http://www.poatransporte.com.br/php/facades/process.php?a=nc&p=%&t=o";
            final Gson gson = new Gson();

            String json = ApiComponent.requisicao(LINK);

            return Stream.of( gson.fromJson(json, Onibus[].class) )
                    .collect(toList());

        } catch (IOException e) {
            e.getMessage();
        }
        return null;
    }
}
