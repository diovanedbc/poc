package br.com.dbc.poatransporte.Repository;

import br.com.dbc.poatransporte.Model.Ponto;
import org.springframework.data.repository.CrudRepository;

import java.math.BigDecimal;
import java.util.List;

public interface PontoRepository extends CrudRepository<Ponto, Long> {

    List<Ponto> findAll();
    List<Ponto> findAllByLat(BigDecimal lat);
    List<Ponto> findAllByLng(BigDecimal lng);
    Ponto findByLatAndLng(BigDecimal lat, BigDecimal lng);

}
