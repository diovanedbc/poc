package br.com.dbc.poatransporte.Component;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

@Component
public abstract class ApiComponent {

    public static String requisicao(String link) throws IOException {
        URL url = new URL( link );
        URLConnection conexao = url.openConnection();

        InputStreamReader inputStreamReader = new InputStreamReader( conexao.getInputStream() );

        BufferedReader buffer = new BufferedReader( inputStreamReader );

        return buffer.readLine();
    }
}
