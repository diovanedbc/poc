package br.com.dbc.poatransporte.Repository;

import br.com.dbc.poatransporte.Model.Cliente;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ClienteRespository extends CrudRepository<Cliente, Long> {

    List<Cliente> findAll();
}
