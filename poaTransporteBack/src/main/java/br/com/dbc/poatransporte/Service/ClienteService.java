package br.com.dbc.poatransporte.Service;

import br.com.dbc.poatransporte.Exception.ClienteException;
import br.com.dbc.poatransporte.Model.Cliente;
import br.com.dbc.poatransporte.Repository.ClienteRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class ClienteService implements ServicePadrao<Cliente>{

    @Autowired
    private ClienteRespository clienteRespository;

    @Transactional(rollbackFor = Exception.class)
    public Cliente salvar(Cliente cliente) {
        return clienteRespository.save(cliente);
    }

    public List<Cliente> buscaTodos() {
        return clienteRespository.findAll();
    }

    public Cliente buscaUm(long id) throws ClienteException {
        return clienteRespository.findById(id)
                .orElseThrow(ClienteException::new);
    }

    @Transactional(rollbackFor = Exception.class)
    public Cliente atualizar(Cliente cliente, long id) {
        cliente.setId(id);
        return clienteRespository.save(cliente);
    }

    @Transactional(rollbackFor = Exception.class)
    public void deletar(Cliente cliente) {
        clienteRespository.delete(cliente);
    }
}
