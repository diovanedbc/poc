package br.com.dbc.poatransporte.Exception;

import java.io.IOException;

public class ConexaoException extends IOException {
    private final String MENSAGEM = "Erro na conexão da API!";

    @Override
    public String getMessage(){
        return MENSAGEM;
    }
}
