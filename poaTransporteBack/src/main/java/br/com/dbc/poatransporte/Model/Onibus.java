package br.com.dbc.poatransporte.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "ONIBUS")
public class Onibus {

    @Id
    private long id;

    @Column(name = "CODIGO")
    private String codigo;

    @Column(name = "NOME")
    private String nome;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "ONIBUS_PONTOS",
        joinColumns = { @JoinColumn(name = "ID_ONIBUS") },
        inverseJoinColumns = { @JoinColumn(name = "ID_PONTO") })
    private List<Ponto> pontos = new ArrayList<>();

    @ManyToMany(mappedBy = "listaOnibus")
    private List<Cliente> clientes = new ArrayList<>();

    public Onibus() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }

    public List<Ponto> getPontos() {
        return pontos;
    }

    public void setPontos(List<Ponto> pontos) {
        this.pontos = pontos;
    }

    public void pushPontos(Ponto... pontos){
        this.pontos.addAll( Arrays.asList(pontos) );
    }

    @Override
    public String toString() {
        return "Onibus{" +
                "id=" + id +
                ", codigo='" + codigo + '\'' +
                ", nome='" + nome + '\'' +
                ", pontos=" + pontos +
                ", clientes=" + clientes +
                '}';
    }
}
