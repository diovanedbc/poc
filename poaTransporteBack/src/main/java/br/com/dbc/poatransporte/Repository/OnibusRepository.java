package br.com.dbc.poatransporte.Repository;

import br.com.dbc.poatransporte.Model.Onibus;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface OnibusRepository extends CrudRepository<Onibus, Long> {

    List<Onibus> findAll();

}
