package br.com.dbc.poatransporte.Model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENTES")
public class Cliente {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "CLIENTE_SEQ", sequenceName = "CLIENTE_SEQ")
    @GeneratedValue(generator = "CLIENTE_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "NOME")
    private String nome;

    @Column(name = "CPF")
    private String cpf;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CLIENTE_ONIBUS",
        joinColumns = { @JoinColumn(name = "ID_CLIENTE") },
        inverseJoinColumns = { @JoinColumn(name = "ID_ONIBUS") })
    private List<Onibus> listaOnibus = new ArrayList<>();

    public Cliente() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public List<Onibus> getListaOnibus() {
        return listaOnibus;
    }

    public void setListaOnibus(List<Onibus> listaOnibus) {
        this.listaOnibus = listaOnibus;
    }
}
