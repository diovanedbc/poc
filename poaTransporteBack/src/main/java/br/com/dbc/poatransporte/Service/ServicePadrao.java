package br.com.dbc.poatransporte.Service;

import br.com.dbc.poatransporte.Exception.ClienteException;

import java.util.List;

public interface ServicePadrao<T> {

   T salvar(T t);
   List<T> buscaTodos();
   T buscaUm(long id) throws ClienteException;
   T atualizar(T t, long id);
   void deletar(T t);

}
