package br.com.dbc.poatransporte;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PoaTransporteApplication {

    public static void main(String[] args) {
        SpringApplication.run(PoaTransporteApplication.class, args);
    }

}
