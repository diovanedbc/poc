package br.com.dbc.poatransporte.Controller;

import br.com.dbc.poatransporte.Model.Onibus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/onibus")
public class OnibusController {

    @GetMapping("/{nome}")
    @ResponseBody
    public Onibus buscaPorNome(@PathVariable String nome){
        return null;
    }
}
