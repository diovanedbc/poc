package br.com.dbc.poatransporte.Component;

import br.com.dbc.poatransporte.Model.Ponto;
import br.com.dbc.poatransporte.Util.PontoParse;
import com.google.gson.Gson;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@Component
public class PontoComponent {

    public List<Ponto> listaRequisicaoPonto(long idOnibus){
        try {
            final String LINK = "http://www.poatransporte.com.br/php/facades/process.php?a=il&p=" + idOnibus;
            final Gson gson = new Gson();

            String json = ApiComponent.requisicao(LINK);

            return Stream.of( json.split(",\"\\d+\":") )
                    .skip(1)
                    .map(e -> formataUltimaString(e))
                    .map(e -> gson.fromJson(e, PontoParse.class))
                    .map(e -> direcaoStringParaNumero(e))
                    .collect(toList());

        } catch (IOException e) {
            e.getMessage();
        }
        return null;
    }

    public String formataUltimaString(String stg){
        int tamanhoStg = stg.length();
        String pegaUltimasLetras = stg.substring(tamanhoStg - 2, tamanhoStg);

        if ( pegaUltimasLetras.equals("}}") )
            stg = stg.replace("}}", "}");

        return stg;
    }

    public Ponto direcaoStringParaNumero(PontoParse pontoParse){
        String lat = pontoParse.getLat();
        String lng = pontoParse.getLng();

        return new Ponto( new BigDecimal(lat), new BigDecimal(lng));
    }

}
