package br.com.dbc.poatransporte.Model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PONTOS")
public class Ponto {

    @Id
    @SequenceGenerator(allocationSize = 1, name = "PONTO_SEQ", sequenceName = "PONTO_SEQ")
    @GeneratedValue(generator = "PONTO_SEQ", strategy = GenerationType.SEQUENCE)
    private long id;

    @Column(name = "LATITUDE")
    private BigDecimal lat;

    @Column(name = "LONGITUDE")
    private BigDecimal lng;

    @ManyToMany(mappedBy = "pontos")
    private List<Onibus> listaOnibus = new ArrayList<>();

    public Ponto() {
    }

    public Ponto(BigDecimal lat, BigDecimal lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLng() {
        return lng;
    }

    public void setLng(BigDecimal lng) {
        this.lng = lng;
    }

    @Override
    public String toString() {
        return "Ponto{" +
                "lat='" + lat + '\'' +
                ", lng='" + lng + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Ponto ponto = (Ponto) o;
        return Objects.equals(lat, ponto.lat) &&
                Objects.equals(lng, ponto.lng);
    }
}
