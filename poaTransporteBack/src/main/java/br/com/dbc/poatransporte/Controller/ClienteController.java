package br.com.dbc.poatransporte.Controller;

import br.com.dbc.poatransporte.Exception.ClienteException;
import br.com.dbc.poatransporte.Model.Cliente;
import br.com.dbc.poatransporte.Service.ClienteService;
import br.com.dbc.poatransporte.Service.OnibusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;
    @Autowired
    private OnibusService onibusService;

    @PostMapping("/cadastro")
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Cliente cadastrar(@RequestBody Cliente cliente){
        return clienteService.salvar(cliente);
    }

    @GetMapping("/")
    @ResponseBody
    public List<Cliente> buscaTodos(){
        return clienteService.buscaTodos();
    }

    @GetMapping("/{id}")
    @ResponseBody
    public Cliente buscaUm(@PathVariable long id){
        try {
            return clienteService.buscaUm(id);
        } catch (ClienteException e) {
            e.getMessage();
        }
        return null;
    }

    @PutMapping("/{id}")
    @ResponseBody
    public Cliente atualizar(@RequestBody Cliente cliente, @PathVariable long id){
        return clienteService.atualizar(cliente, id);
    }

    @DeleteMapping("/{id}")
    public void deletar(@RequestBody Cliente cliente){
        clienteService.deletar(cliente);
    }
}
