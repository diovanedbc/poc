package br.com.dbc.poatransporte.Exception;

public class ClienteException extends Exception{
    private final String MENSAGEM = "Cliente não consiste no sistema!";

    @Override
    public String getMessage(){
        return MENSAGEM;
    }
}
