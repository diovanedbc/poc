package br.com.dbc.poatransporte.Service;

import br.com.dbc.poatransporte.Component.OnibusComponent;
import br.com.dbc.poatransporte.Component.PontoComponent;
import br.com.dbc.poatransporte.Model.Onibus;
import br.com.dbc.poatransporte.Model.Ponto;
import br.com.dbc.poatransporte.Repository.OnibusRepository;
import br.com.dbc.poatransporte.Repository.PontoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;

@Service
public class OnibusService {

    @Autowired
    private OnibusRepository onibusRepository;

    @Autowired
    private PontoRepository pontoRepository;

    private OnibusComponent onibusComponent = new OnibusComponent();

    private PontoComponent pontoComponent = new PontoComponent();

    @Transactional(rollbackFor = Exception.class)
    public void carregarBanco(){
        //Ponto{lat='-30.03191856832800000', lng='-51.22357813355400000'}

        List<Onibus> listaOnibus =  onibusComponent.listaRequisicaoBus();
        List<Long> idsBus = listaOnibus.stream()
                .map(Onibus::getId)
                .collect(toList());

        //a.forEach(System.out::println);



        for (Onibus bus : onibusComponent.listaRequisicaoBus()){
            onibusRepository.save(bus);

            bus.setPontos(pontoComponent.listaRequisicaoPonto(bus.getId()));

            onibusRepository.save(bus);
        }


    }
}
